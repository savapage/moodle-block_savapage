<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <https://www.gnu.org/licenses/>.

/**
 * Block for Single sign-on to SavaPage User Web App.
 *
 * @package block_savapage
 * @copyright &copy; 2016 onwards Datraverse B.V. {@link https://www.datraverse.com/}
 * @license https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Rijk Ravestein <rijkr@datraverse.com>
 */
defined ( 'MOODLE_INTERNAL' ) || die ();

/**
 * Display the sign-in button.
 */
class block_savapage extends block_base {
    public function init() {
        $this->title = get_string ( 'savapageblocktitle', 'block_savapage' );
    }
    
    // only one instance of this block is required
    public function instance_allow_multiple() {
        return false;
    }
    
    // label and button values can be set in admin
    public function has_config() {
        return true;
    }
    public function get_content() {
        if (null !== $this->content) {
            return $this->content;
        }
        
        $this->content = new stdClass ();
        
        $url = new moodle_url ( '/blocks/savapage/view.php', array (
            'blockid' => $this->instance->id 
        ) );
        
        $this->content->text = '<form action="' . $url->__toString () . '" target="_blank" method="post">';
        $this->content->text .= '<input type="submit" value="' . get_string ( 'savapage_user_web_app', 'block_savapage' ) . '" />';
        $this->content->text .= '</form>';
        
        // Use var_export($USER) to view all
        $this->content->footer = '';
        
        return $this->content;
    }
    
    /**
     * Which page types this block may appear on.
     *
     * @return array page-type prefix => true/false.
     */
    function applicable_formats() {
        return array (
            'all' => true 
        );
    }
    
    // Hide this block from non-authorized users
    public function is_empty() {
        global $USER;
        
        return ! isset ( $USER->auth );
    }
}
// end-of-file
