<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Settings for block_savapage.
 * 
 * @package block_savapage
 * @copyright &copy; 2016 onwards Datraverse B.V. {@link https://www.datraverse.com/}
 * @license https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Rijk Ravestein <rijkr@datraverse.com>
 */
defined ( 'MOODLE_INTERNAL' ) || die ();

$settings->add(new admin_setting_configtext('block_savapage/host',
        get_string('savapage_host', 'block_savapage'),
        get_string('savapage_host_desc', 'block_savapage'),
        'savapage'));

$settings->add(new admin_setting_configtext('block_savapage/ssl_port',
        get_string('savapage_ssl_port', 'block_savapage'),
        get_string('savapage_ssl_port_desc', 'block_savapage'),
        '8632'));

$settings->add(new admin_setting_configcheckbox('block_savapage/ssl_verify_peer',
        get_string('savapage_ssl_verify_peer', 'block_savapage'),
        get_string('savapage_ssl_verify_peer_desc', 'block_savapage'),
        0));

$settings->add(new admin_setting_configcheckbox('block_savapage/ssl_verify_host',
        get_string('savapage_ssl_verify_host', 'block_savapage'),
        get_string('savapage_ssl_verify_host_desc', 'block_savapage'),
        1));

$settings->add(new admin_setting_configtext('block_savapage/webapp_url_path',
        get_string('savapage_webapp_url_path', 'block_savapage'),
        get_string('savapage_webapp_url_path_desc', 'block_savapage'),
        'user'));

$settings->add(new admin_setting_configtext('block_savapage/xmlrpc_url_path',
        get_string('savapage_xmlrpc_url_path', 'block_savapage'),
        get_string('savapage_xmlrpc_url_path_desc', 'block_savapage'),
        'xmlrpc'));

$settings->add(new admin_setting_configtext('block_savapage/ttp_key',
        get_string('savapage_ttp_key', 'block_savapage'),
        get_string('savapage_ttp_key_desc', 'block_savapage'),
        ''));

// end-of-file
