<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <https://www.gnu.org/licenses/>.

/**
 * Hidden input fields with retrieved SSO token and auto-submit to SavaPage User Web App.
 *
 * @package block_savapage
 * @copyright &copy; 2016 onwards Datraverse B.V. {@link https://www.datraverse.com/}
 * @license https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Rijk Ravestein <rijkr@datraverse.com>
 */

include_once ("locallib.php");

try {
    $_userid = $USER->username;
    $_authtoken = block_savapage_create_token ( get_config ( 'block_savapage', 'ttp_key' ), $_userid );
} catch ( Exception $e ) {
    echo "Exception: " . $e->getMessage ();
    die ();
}

?>
<html>
<head>
<script type="text/javascript">          
            function block_savapage_submit_to_webapp() { 
                document.forms["block_savapage_user_web_app"].submit(); 
            };            
        </script>
</head>

<body onload="block_savapage_submit_to_webapp()">
	<form name="block_savapage_user_web_app" method="post"
		action="<?php echo block_savapage_get_url(get_config('block_savapage', 'webapp_url_path'))."?sp-user=".$_userid; ?>">
		<input type="hidden" name="auth_user" value="<?php echo $_userid; ?>"> <input
			type="hidden" name="auth_token" value="<?php echo $_authtoken; ?>">
	</form>
</body>

</html>
