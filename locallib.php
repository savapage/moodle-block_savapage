<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin functions.
 *
 * @package block_savapage
 * @copyright &copy; 2016 onwards Datraverse B.V. {@link https://www.datraverse.com/}
 * @license https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Rijk Ravestein <rijkr@datraverse.com>
 */

require_once ('../../config.php');

// https://github.com/gggeek/phpxmlrpc/releases/tag/3.0.1
include_once ("xmlrpc.inc");

// Composes a URL to SavaPage server.
function block_savapage_get_url($path) {
    $url = "https://" . get_config ( 'block_savapage', 'host' );
    $port = get_config ( 'block_savapage', 'ssl_port' );
    if (isset ( $port ) && strlen ( trim ( $port ) ) > 0) {
        $url .= ":" . $port;
    }
    $url .= "/" . $path;
    return $url;
}

// Gets the one-time authentication createToken
function block_savapage_create_token($ttpkey, $userid, $timeoutSecs = 0) {
    $client = new xmlrpc_client ( block_savapage_get_url ( get_config ( 'block_savapage', 'xmlrpc_url_path' ) ) );
    $client->setSSLVerifyPeer ( get_config ( 'block_savapage', 'ssl_verify_peer' ) );
    $client->setSSLVerifyHost ( get_config ( 'block_savapage', 'ssl_verify_host' ) );

    $r = $client->send ( new xmlrpcmsg ( 'onetime-auth.createToken', array (
        php_xmlrpc_encode ( $ttpkey ),
        php_xmlrpc_encode ( $userid )
    ) ), $timeoutSecs );

    if ($r->faultCode ()) {
        print "An error occurred: ";
        print "<pre>Code: " . htmlspecialchars ( $r->faultCode () ) . " Reason: '" . htmlspecialchars ( $r->faultString () ) . "'</pre><br/>";
        die ();
    }

    return $r->value ()->scalarval ();
}
