# moodle-block_savapage

Single sign-on to SavaPage User Web App from Moodle learning platform.

This block shows a button on the Moodle dashboard to open the SavaPage Libre Print Management Web App, so printed documents can be previewed and forwarded. The credentials of the authenticated Moodle user are trusted so no extra login is needed (single sign-on).
 
### License

This code block is part of Moodle - [https://moodle.org/](https://moodle.org/)

Moodle is free software: you can redistribute it and/or modify
it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html)
as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
The software is provided by the SavaPage project <https://www.savapage.org>,
copyright (c) 2011-2016 Datraverse B.V.

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Fellows. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Source Code

[https://gitlab.com/savapage/moodle-block_savapage](https://gitlab.com/savapage/moodle-block_savapage)  

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Help Desk

[https://support.savapage.org](https://support.savapage.org)

### Third Party Components 

`xmlrpc.inc` is (c) 1999-2002 by Edd Dumbill <edd@usefulinc.com>


### Supported Moodle versions

Moodle 3.0

### Dependencies

CURL must be supported by your PHP Moodle environment. In Debian based systems with PHP 7 installed, CURL can be installed and activated with these commands:

    sudo apt-get install php7.0-curl
    sudo service apache2 reload

### Installation

Install this block in Moodle and configure the parameters as discussed below. Please consult your SavaPage administrator for the right values.

__Host__
* Hostname or IP address of the SavaPage server.

__SSL Port__
* SSL port number  of the SavaPage server.

__SSL Verify Peer__
* Enable/disable verification of peer certificate. Disable to accept self-signed certificate.

__SSL Verify Host__
* Enable/disable verification of match between server certificate CN and URL hostname.

__WebApp Path__
* URL path of the SavaPage User Web App.

__XML-RPC Path__
* URL path of the SavaPage XML-RPC API.

__TTP Key__
* The Trusted Third Party API key.
 


